export class User {
  constructor(public pseudo: string, public numberOfClicks: number, public stack: number = 0) { }
}
