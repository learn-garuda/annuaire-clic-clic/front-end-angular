import { Component, OnInit } from '@angular/core';
import {User} from "../../models/User";
import {UserService} from "../services/user.service";

@Component({
  selector: 'app-ladder',
  templateUrl: './ladder.component.html',
  styleUrls: ['./ladder.component.sass']
})
export class LadderComponent implements OnInit {

  public currentUser: User = new User('', 0);

  public users: Array<User>;

  // @ts-ignore
  public senderClick: number = undefined;

  constructor(private userService: UserService) {
    this.users = new Array<User>();


    userService.getInfos()
      .then(response =>
        // @ts-ignore
        this.currentUser = new User(response['body']['pseudo'], response['body']['numberOfClicks'])
      )
      .catch(console.log);
  }

  ngOnInit(): void {
    this.loadUsers();
  }

  public loadUsers(): void {
    this.users = new Array<User>();

    this.userService.getAll()
      .then(response =>
        // @ts-ignore
        response['body']['users'].forEach(user =>
          this.users.push(new User(user['pseudo'], user['numberOfClicks']))
        )
      ).catch(console.log);
  }

  public onClick(): void {

    this.currentUser.stack += 1;

    if (this.senderClick !== undefined) {
      clearTimeout(this.senderClick)
    }

    this.senderClick = setTimeout(() =>
      this.userService.click(this.currentUser.stack)
        .then(response => {
          // @ts-ignore
          this.currentUser.numberOfClicks = response['body']['numberOfClicks'];
          this.currentUser.stack = 0;

        }).then(() =>
          // @ts-ignore
          this.senderClick = undefined
        ).catch(console.log), 2000);
  }
}
