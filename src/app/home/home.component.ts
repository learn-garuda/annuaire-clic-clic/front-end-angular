import { Component, OnInit } from '@angular/core';
import {faMouse, faMousePointer} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  public faMouse = faMouse;
  public faMousePointer = faMousePointer;

  constructor() { }

  ngOnInit(): void { }
}
