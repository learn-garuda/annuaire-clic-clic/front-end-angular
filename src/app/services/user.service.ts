import { Injectable } from '@angular/core';
import {AbstractService} from "./abstract.service";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class UserService extends AbstractService {

  constructor(private httpClient: HttpClient) {
    super(httpClient);
  }

  public getAll() {
    return this.getService('/user/ladder');
  }

  public getInfos() {
    return this.getService('/user/info');
  }

  public click(clicksToAdd: number = 1) {
    return this.postService('/user/click', {
        "clicksToAdd": clicksToAdd
    });
  }
}
