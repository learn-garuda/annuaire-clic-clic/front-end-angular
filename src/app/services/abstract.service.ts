import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";

export abstract class AbstractService {

  protected constructor(private http: HttpClient) { }

  protected postService(endpoint: string, body: object): Promise<Object> {
    return new Promise<Object>((resolve, reject) => {
      this.http.post(`${environment.server}${endpoint}`, body, {
        observe: 'response',
        headers: new HttpHeaders({'Content-Type': 'application/json'}),
        withCredentials: true
      }).subscribe({
        next: data => resolve(data),
        error: data => reject(data)
      });
    });
  }

  protected deleteService(endpoint: string): Promise<Object> {
    return new Promise<Object>((resolve, reject) => {
      this.http.delete(`${environment.server}${endpoint}`, {
        observe: 'response',
        withCredentials: true
      }).subscribe({
        next: data => resolve(data),
        error: data => reject(data)
      });
    });
  }

  protected getService(endpoint: string): Promise<Object> {
    return new Promise<Object>((resolve, reject) => {
      this.http.get(`${environment.server}${endpoint}`, {
        observe: 'response',
        withCredentials: true
      }).subscribe({
        next: data => resolve(data),
        error: data => reject(data)
      });
    });
  }
}
