import {Injectable} from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router'
import {HttpClient} from "@angular/common/http";
import {AuthService} from "./auth.service";

@Injectable()
export class AuthGuardService implements CanActivate {

  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Promise<boolean | UrlTree> {


    return Promise.resolve(
      this.authService.isAuth()
        .then(response => {
          // @ts-ignore
          if (!response['body']['isAuth']) {
            this.router.navigate(['home']);
            return false;
          }
          return true;
        }).catch(() => false)
    );
  }

  constructor(private router: Router, private http: HttpClient, private authService: AuthService) { }
}
