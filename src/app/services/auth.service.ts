import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AbstractService} from "./abstract.service";

@Injectable({
  providedIn: 'root'
})
export class AuthService extends AbstractService {

  constructor(private httpClient: HttpClient) {
    super(httpClient);
  }

  public login(pseudo: string, password: string) : Promise<Object> {
    return this.postService('/user/login', {
      pseudo: pseudo,
      password: password
    });
  }

  public logout() : Promise<Object> {
    return this.deleteService('/user/logout');
  }

  public isAuth() : Promise<Object> {
    return this.getService('/user/isAuth');
  }

  public deleteAccount() : Promise<Object> {
    return this.deleteService('/user/delete');
  }

  public createAccount(pseudo: string, password: string) : Promise<Object> {
    return this.postService('/user/create', {
      pseudo: pseudo,
      password: password
    });
  }
}
