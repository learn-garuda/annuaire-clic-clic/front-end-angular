import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {faAddressBook, faTrophy} from "@fortawesome/free-solid-svg-icons";
import {AuthService} from "../services/auth.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {

  public faAddressBook = faAddressBook;
  public faTrophy = faTrophy;

  public isLogin: boolean = false;

  // Modal
  public display = 'one';
  public modelTitle = 'Login';

  @ViewChild('LoginPopUp', {static: false}) LoginPopUp: ElementRef<HTMLElement> | undefined;
  @ViewChild('CreateAccountPopUp', {static: false}) CreatePopUp: ElementRef<HTMLElement> | undefined;

  public FormGroup = new FormGroup({
    Pseudo: new FormControl('', Validators.required),
    Password: new FormControl('', Validators.required)
  });

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    // Detect if the user is authenticated or not
    this.authService.isAuth()
      .then(response => {

        // @ts-ignore
        this.isLogin = response['body']['isAuth'];

      })
      .catch(() => this.isLogin = false);
  }

  public onLoginClickAction(): void {
    this.authService.login(
      this.FormGroup.get('Pseudo')?.value,
      this.FormGroup.get('Password')?.value
    )
      .then(() => this.onCloseHandled())
      .then(() =>
        this.router.navigate(['/ladder'])
          .then(() => this.isLogin = true)
          .catch(console.log)
      ).catch(response => alert(response['error']['message']));
  }

   public onCreateClickAction(): void {
     this.authService.createAccount(
       this.FormGroup.get('Pseudo')?.value,
       this.FormGroup.get('Password')?.value
     )
       .then(() => this.onCloseHandled())
       .catch(response => alert(response['error']['message']));
   }

  public onDeleteAccountClick(): void {
    this.authService.deleteAccount()
      .then(() =>
        this.router.navigate(['/home'])
          .then(() => this.isLogin = false)
          .catch(console.log)
      ).catch(console.log);
  }

  public onLogoutClick(): void {
    this.authService.logout()
      .then(() => {
        this.router.navigate(['/home'])
            .then(() => this.isLogin = false)
            .catch();
      }).catch((response) => alert(response['error']['message']));
  }

  public openModal(title: string): void {
    this.modelTitle = title;
    this.display = "block";
  }

  public onCloseHandled(): void {
    this.display = 'none';
    this.FormGroup.patchValue({
      Pseudo: '',
      Password: ''
    });
  }
}
